<!DOCTYPE html>
<html>
	<body>
		<h1>Manipulasi waktu</h1>
		<?php
            $timestamp = time();
            $converted = date("D, d M Y H:i:s", time());
            echo "Waktu Sekarang: ".$converted;
            echo "<br>Timestamp-nya: ".$timestamp;
        ?>
		<hr>
		<?php if(!isset($_POST['submit'])) : ?>
			<form action="manipulasi_waktu.php" name="form" method="post">
				<input type="numeric" min="1970" name="tahun" placeholder="YYYY"/><br>
				<input type="numeric" max="12" name="bulan" placeholder="MM"/><br>
				<input type="numeric" max="31" name="tanggal" placeholder="DD"/><br>
				<select name="operator">
					<option value="+" selected>Tambah</option>
					<option value="-">Kurang</option>
				</select><br>
				<input type="numeric" name="lamanya"/><br>
				<select name="jenis">
					<option value="years" selected>Tahun</option>
					<option value="month">Bulan</option>
					<option value="day">Hari</option>
					<option value="hour">Jam</option>
					<option value="minute">Menit</option>
					<option value="second">Detik</option>
				</select><br>
				<input type="submit" name="submit"/>
			</form>
		<?php else: ?> 
			<form action="manipulasi_waktu.php" name="form" method="post">
				<input type="numeric" min="1970" name="tahun" placeholder="YYYY"/><br>
				<input type="numeric" max="12" name="bulan" placeholder="MM"/><br>
				<input type="numeric" max="31" name="tanggal" placeholder="DD"/><br>
				<select name="operator">
					<option value="+" selected>Tambah</option>
					<option value="-">Kurang</option>
				</select><br>
				<input type="numeric" name="lamanya"/><br>
				<select name="jenis">
					<option value="years" selected>Tahun</option>
					<option value="month">Bulan</option>
					<option value="day">Hari</option>
					<option value="hour">Jam</option>
					<option value="minute">Menit</option>
					<option value="second">Detik</option>
				</select><br>
				<input type="submit" name="submit"/>
			</form>
		<?php
			//inisialisasi
			$tanggal 	= $_POST['tanggal'];
			$bulan 		= $_POST['bulan'];
			$tahun 		= $_POST['tahun'];
			$operator	= $_POST['operator'];
			$lamanya	= $_POST['lamanya'];
			$jenis		= $_POST['jenis'];
			$tmpWaktu 	= $tahun.'-'.$bulan.'-'.$tanggal;
			$strtotime 	= strtotime($tmpWaktu);
			$waktu 		= date("D, d M Y H:i:s", $strtotime);
			$input 		= date("Y-m-d", $strtotime);
            echo "<p>Waktunya: ".$waktu."</p>";
			echo "<p>Timestamp-nya: ".$strtotime."</p>";
            echo "<p>Waktu yang diinput: ".$input." ".$operator."".$lamanya." ".$jenis."</p>";
			
			//manipulasi pake date format
			$modify		= $operator.''.$lamanya.' '.$jenis;
			$date 		= DateTime::createFromFormat('Y-m-d',$tmpWaktu);
			$date->modify($modify);
			echo "<p>Setelah dimanipulasi: ".$date->format('Y-m-d')."</p>";
			echo "<hr>";
			echo "<pre>";
			echo "</pre>";
		endif;
		?>
		<hr><a href="index.php">Kembali</a>
	</body>
</html>