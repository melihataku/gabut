<!DOCTYPE html>
<html>
	<body>
		<h1>Bikin timestamp</h1>
		<?php
            $timestamp = time();
            $converted = date("D, d M Y H:i:s", time());
            echo "Waktu Sekarang: ".$converted;
            echo "<br>Timestamp-nya: ".$timestamp;
        ?>
		<hr>
		<?php if(!isset($_POST['submit'])) : ?>
			<form action="bikin_timestamp.php" name="form" method="post">
				<input type="numeric" min="1970" name="tahun" placeholder="YYYY"/><br>
				<input type="numeric" max="12" name="bulan" placeholder="MM"/><br>
				<input type="numeric" max="31" name="tanggal" placeholder="DD"/><br>
				<input type="submit" name="submit"/>
			</form>
		<?php else: ?> 
			<form action="bikin_timestamp.php" name="form" method="post">
				<input type="numeric" min="1970" name="tahun" placeholder="YYYY"/><br>
				<input type="numeric" max="12" name="bulan" placeholder="MM"/><br>
				<input type="numeric" max="31" name="tanggal" placeholder="DD"/><br>
				<input type="submit" name="submit"/>
			</form>
		<?php
			$tanggal 	= $_POST['tanggal'];
			$bulan 		= $_POST['bulan'];
			$tahun 		= $_POST['tahun'];
			$tmpWaktu 	= $tanggal.'-'.$bulan.'-'.$tahun;
			$strtotime 	= strtotime($tmpWaktu);
			$waktu 		= date("D, d M Y H:i:s", $strtotime);
		?>
		<p>Waktunya: <?php echo $waktu ?></p>
		<p>Timestamp-nya: <?php echo $strtotime ?></p>
		<p>Referensi: <a href="https://www.php.net/manual/en/datetime.format.php">Dokumentasi DateTime</a>
		<pre>
			<table>
				<tr>
					<th width="20">Format Karakter</th>
					<th width="60">Hasilnya</th>
				</tr>
				<tr>
					<td width="20">d</td>
					<td width="60"><?= date("d", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">D</td>
					<td width="60"><?= date("D", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">j</td>
					<td width="60"><?= date("j", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">l (L kecil)</td>
					<td width="60"><?= date("l", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">N</td>
					<td width="60"><?= date("N", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">S</td>
					<td width="60"><?= date("S", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">w</td>
					<td width="60"><?= date("w", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">Z</td>
					<td width="60"><?= date("Z", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">W</td>
					<td width="60"><?= date("W", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">F</td>
					<td width="60"><?= date("F", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">m</td>
					<td width="60"><?= date("m", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">M</td>
					<td width="60"><?= date("M", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">n</td>
					<td width="60"><?= date("n", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">t</td>
					<td width="60"><?= date("t", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">L</td>
					<td width="60"><?= date("L", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">o</td>
					<td width="60"><?= date("o", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">Y</td>
					<td width="60"><?= date("Y", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">y</td>
					<td width="60"><?= date("y", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">a</td>
					<td width="60"><?= date("a", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">A</td>
					<td width="60"><?= date("A", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">B</td>
					<td width="60"><?= date("B", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">g</td>
					<td width="60"><?= date("g", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">G</td>
					<td width="60"><?= date("G", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">h</td>
					<td width="60"><?= date("h", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">H</td>
					<td width="60"><?= date("H", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">i</td>
					<td width="60"><?= date("i", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">s</td>
					<td width="60"><?= date("s", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">u</td>
					<td width="60"><?= date("u", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">v</td>
					<td width="60"><?= date("v", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">e</td>
					<td width="60"><?= date("e", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">I (i kapital)</td>
					<td width="60"><?= date("I", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">O</td>
					<td width="60"><?= date("O", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">P</td>
					<td width="60"><?= date("P", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">t</td>
					<td width="60"><?= date("t", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">Z</td>
					<td width="60"><?= date("Z", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">c</td>
					<td width="60"><?= date("c", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">r</td>
					<td width="60"><?= date("u", $strtotime) ?></td>
				</tr>
				<tr>
					<td width="20">U</td>
					<td width="60"><?= date("U", $strtotime) ?></td>
				</tr>
			</table>
		</pre>
		<?php endif; ?>
		<hr><a href="index.php">Kembali</a>
	</body>
</html>